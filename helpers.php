<?php

if( ! function_exists('assets_manifest')){
    /**
     * Return the hashed name asset from the manifest.json
     *
     * @param $assetPath
     * @return string
     * @throws Exception
     */
    function asset_manifest($assetPath){

        $pathToManifest = public_path('manifest.json');

        if(file_exists($pathToManifest)) {
            $assetArray = json_decode(file_get_contents($pathToManifest), true);
            if (isset($assetArray[$assetPath])) {
                return asset($assetArray[$assetPath]);
            }
            throw new Exception("The provided key asset doesn't exist n the manifest.");
        }
        throw new Exception('Unable to locate the manifest.json.');
    }

}

if( ! function_exists('rand_hexa_color')){
    /**
     * Return a random hexa color with or without a prepend hashtag
     *
     * @param bool $withHash
     * @return string
     */
    function rand_hexa_color(bool $withHash = false) : string {
        $hexColor = $withHash ? '#' : '';
        $hexColor .= dechex(mt_rand(0, 16777215));

        return $hexColor;
    }
}